import 'dart:js';

import 'package:flutter/material.dart';

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: const EdgeInsets.only(
                bottom: 8,
              ),
              child: Text(
                'Oeschinen Lake Campgroud',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          Text('Kandersteg, Switzerland',
              style: TextStyle(color: Colors.grey[500]))
        ],
      )),
      Icon(
        Icons.star,
        color: Colors.red[500],
      ),
      Text('41')
    ],
  ),
);

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
          _buildButtonColumn(color, Icons.share, 'SHARE'),
        ],
      ),
    );
    return MaterialApp(
        title: 'Flutter Layout demo',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Flutter Layout demo'),
          ),
          body: Column(
            children: [titleSection],
            children: [
              titleSection,
              buttonSection,
            ],
          ),
        ));
  }
}
